##Para importar o projeto, utilizar da seguinte forma:  ##
Adicionar o repositorio dentro de allprojects do build.gradle do projeto:

```
#!text
maven {
  url "https://api.bitbucket.org/1.0/repositories/forusers/core-android/raw/releases"
}


```
  
Adicionar dentro de dependencies do build.gradle do app:

```
#!text

compile 'br.com.forusers.core.android:core-android:0.0.13'
```


# Configurar ambiente para realizar alterações na library#
Para configurar o ambiente para fazer uploadArchives é necessário criar uma [chave de acesso do ssh](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html).Solicitar ao responsável do projeto através do email: maiconhellmann@gmail.com.  
  
###Algumas erros encontrados ao configurar:###
* Permissão do arquivo ~./.ssh/id_rsa, utilizar 600.
* USERNAME e PASSWORD no gradle.properties na raiz do projeto. Pois este arquivo não é enviado ao git por conter usuário e senha do bitbucket
* Tutorial: http://jeroenmols.com/blog/2016/02/05/wagongit/
* Editar o arquivo ./gradlew e adicionar a seguinte linha:  
rm -R -f /tmp/*
* Verificar se o agente SSH está ativo:
ps -e | grep [s]sh-agent 
* Executar o agente SSH
ssh-agent /bin/bash
* Atribuir id ao agente
ssh-add ~/.ssh/id_rsa