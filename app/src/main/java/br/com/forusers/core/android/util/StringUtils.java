package br.com.forusers.core.android.util;

import java.text.*;

public class StringUtils {

    public StringUtils() {
    }

    public static boolean hasValue(String string) {
        return string == null? false : !string.trim().isEmpty();
    }

    public static String checkNullStr(String string) {
        return hasValue(string) && string.equalsIgnoreCase("null")?null:string;
    }

    public static String removeDiacriticalMarks(String string) {
        return Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    public static boolean existInString(String string, String query) {
        if(!hasValue(string)) {
            return false;
        } else {
            string = removeDiacriticalMarks(string.toLowerCase());
            query = removeDiacriticalMarks(query.toLowerCase());
            return string.contains(query);
        }
    }

    public static boolean isNullOrEmpty(Object obj) {
        if(obj != null && obj instanceof String) {
            String str = (String)obj;
            return str.trim().isEmpty();
        } else {
            return true;
        }
    }
}

