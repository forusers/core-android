package br.com.forusers.core.android.component;

import android.content.*;
import android.util.*;

import com.afollestad.materialdialogs.*;

import br.com.forusers.core.android.R;

public class DialogBuilder {

    public static void e(Context context, Throwable t){
        Log.e(DialogBuilder.class.getSimpleName(), context.getString(R.string.erro),t);
        String message = t.getMessage() != null ? t.getMessage() : t.getClass().getName();
        showDialog(context, context.getString(br.com.forusers.core.android.R.string.erro), message);
    }
    public static void e(Context context,String title, Throwable t){
        Log.e(DialogBuilder.class.getSimpleName(), context.getString(R.string.erro),t);
        String message = t.getMessage() != null ? t.getMessage() : t.getClass().getName();
        showDialog(context, title, message);
    }

    public static void m(Context context,String title, String message){
        showDialog(context,title, message);
    }
    public static void m(Context context, String message){
        showDialog(context, context.getString(R.string.mensagem), message);
    }

    public static void m(Context context, int titleResourceString, int messageResourceString){
        m(context, context.getString(titleResourceString), context.getString(messageResourceString));
    }

    public static void w(Context context,String title, String message){
        showDialog(context,title, message);
    }
    public static void w(Context context,String message){
        showDialog(context,context.getString(R.string.aviso), message);
    }
    public static void w(Context context, int titleResourceString, int messageResourceString){
        w(context, context.getString(titleResourceString), context.getString(messageResourceString));
    }

    private static void showDialog(Context context, String title, String message){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
        if(title != null && !title.trim().equals(""))
            builder.title(title);

        builder.content(message);
        builder.positiveText("Fechar");
        builder.show();
    }
}
