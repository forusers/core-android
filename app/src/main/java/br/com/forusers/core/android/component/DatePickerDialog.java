package br.com.forusers.core.android.component;

import android.app.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.*;
import android.widget.*;

import java.util.*;

import br.com.forusers.core.android.*;


/**
 * Utilização:
 private void onClickDataFaturamentro() {
     try {
             DatePickerDialog datePicker = new DatePickerDialog();
             datePicker.setListener(new DatePickerDialog.DatePickerServiceListener() {
             public void onSelectDate(Date date) throws Exception {
             // ...
         }
         });
         datePicker.show(getFragmentManager(), DatePickerDialog.class.getSimpleName());
     } catch (Exception e) {
     DialogBuilder.e(getContext(), e);
     }
 }
 */
public class DatePickerDialog extends DialogFragment implements android.app.DatePickerDialog.OnDateSetListener {


    public DatePickerDialog() {
    }

    public interface DatePickerServiceListener {
        void onSelectDate(Date date) throws Exception;
    }
    private DatePickerServiceListener listener;
    private android.app.DatePickerDialog dialog;
    private Date date;

    public void setListener(DatePickerServiceListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();

        if (date != null) {
            c.setTime(date);
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        dialog = new android.app.DatePickerDialog(getActivity(), this, year, month, day);

        dialog.show();
        try {
            final Button positive = dialog.getButton(android.app.DatePickerDialog.BUTTON_POSITIVE);
            positive.setText(R.string.ok);
        } catch (Exception e) {
            Log.e(getClass().getName(), "Erro ao alterar label do datePicker", e);
        }

        return dialog;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
        if (dialog != null) {
            try {
                final Button positive = dialog.getButton(android.app.DatePickerDialog.BUTTON_POSITIVE);
                positive.setText(R.string.ok);
            } catch (Exception e) {
                Log.e(getClass().getName(), "Erro ao alterar label do datePicker", e);
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);

            listener.onSelectDate(calendar.getTime());
        } catch (Exception e) {
            Log.e(getClass().getName(), "Erro ao atribuir data.", e);
        }
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
