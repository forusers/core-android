package br.com.forusers.core.android.common.fragment;

import android.os.*;
import android.support.annotation.*;
import android.support.design.widget.*;
import android.support.v4.app.*;
import android.util.*;
import android.view.*;

import br.com.forusers.core.android.*;
import br.com.forusers.core.android.component.*;

public abstract class AbstractDialogFragment extends DialogFragment {
    protected final String TAG = this.getClass().getSimpleName();
    private View layoutProgressBar;
    protected String name;
    protected int layoutId;
    protected FloatingActionButton fab;

    protected Exception exception;

    public AbstractDialogFragment(@LayoutRes int layoutId) {
        this.layoutId=layoutId;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(this.layoutId, container, false);

        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            this.fab = (FloatingActionButton) view.getRootView().findViewById(R.id.defaultFab_fab);
            showFab(false);
            this.layoutProgressBar = view.findViewById(R.id.layoutProgressBar);
        } catch (Exception e) {
            DialogBuilder.e(getContext(), e);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.init();
    }

    protected void init() {
        this.initComponent(this.getView());
        this.initListeners();
        this.initData();
    }

    public abstract void initComponent(View var1);
    public abstract void initListeners();
    public abstract void initData();

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void showProgressBar(Boolean show) {
        if(this.layoutProgressBar != null) {
            if(show) {
                this.layoutProgressBar.setVisibility(View.VISIBLE);
            } else {
                this.layoutProgressBar.setVisibility(View.GONE);
            }
        } else {
            Log.d(this.getClass().getSimpleName(), "showProgressBar: Tela não possui progressBar");
        }

    }

    public void showFab(boolean show){
        if(fab != null) {
            fab.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
}

