package br.com.forusers.core.android.component;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.pinball83.maskededittext.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Exibe dialog com componente MaskedEditText para utilizar máscaras nos campos.
 */
public class MaskedInputDialog {

    //Componentes
    private TextView tvHint;
    private MaskedEditText edText;
    private View rootView;

    //Valores dos componentes
    private String hint;
    private String mask;
    private String input;
    private String notMaskedSymbol;
    private final String title;

    //Utilizado para verificar se o usuário informou todos os valores
    private int symbolCount;

    private final Context context;

    public static final String MASK_PHONE_10="(**)****-****";
    public static final String MASK_PHONE_11="(**)*****-****";
    public static final String MASK_CNPJ="**.***.***/****-**";
    public static final String MASK_CPF="***.***.***-**";
    public static final String MASK_CEP="*****-***";
    public static final String DEFAULT_SYMBOL="*";
    private MaterialDialog materialDialog;

    public interface MaskedInputListener{
        void onInputMaskedValue(String value);
        void onInputValue(String value);
    }

    public MaskedInputDialog(Context context, String title, String hint, String input){
        this.title = title;
        this.context = context;
        this.hint = hint;
        this.input=input;
    }

    public void showCnpjInputDialog(MaskedInputListener listener){
        init(MASK_CNPJ, DEFAULT_SYMBOL, listener, InputType.TYPE_CLASS_NUMBER);
    }
    public void showCpfInputDialog(MaskedInputListener listener){
        init(MASK_CPF, DEFAULT_SYMBOL, listener, InputType.TYPE_CLASS_NUMBER);
    }
    public void showPhone10InputDialog(MaskedInputListener listener){
        init(MASK_PHONE_10, DEFAULT_SYMBOL, listener, InputType.TYPE_CLASS_NUMBER);
    }
    public void showPhone11InputDialog(MaskedInputListener listener){
        init(MASK_PHONE_11, DEFAULT_SYMBOL, listener, InputType.TYPE_CLASS_NUMBER);
    }
    public void showCepInputDialog(MaskedInputListener listener){
        init(MASK_CEP, DEFAULT_SYMBOL, listener, InputType.TYPE_CLASS_NUMBER);
    }

    public void show(String mask, String notMaskedSymbol,MaskedInputListener listener, int inputType){
        init(mask, notMaskedSymbol, listener, inputType);
    }

    private void init(String mask, String notMaskedSymbol, final MaskedInputListener listener, int inputType){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
        builder.title(title);
        builder.customView(br.com.forusers.core.android.R.layout.masked_input_dialog, false);
        builder.negativeText("CANCELAR");
        builder.positiveText("OK");
        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                if(edText != null && edText.getText() != null){
                    listener.onInputValue(edText.getUnmaskedText());
                    listener.onInputMaskedValue(edText.getText().toString());
                }
            }
        });
        materialDialog = builder.show();
        rootView = materialDialog.getCustomView();


        //Não permite confirmar
        materialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);

        this.mask = mask;
        this.notMaskedSymbol = notMaskedSymbol;
        this.symbolCount = getSymbolCount();

        if(rootView != null) {
            tvHint = (TextView) rootView.findViewById(br.com.forusers.core.android.R.id.maskedImputMs_hint);
            tvHint.setText(hint);

            edText = new MaskedEditText(context, mask, notMaskedSymbol);
            edText.setInputType(inputType);
            edText.setMaskedText(input);
            ((LinearLayout)rootView).addView(edText);

            //Evento do botão OK/DONE
            edText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    boolean isValidValue = isValidValue(edText.getUnmaskedText());

                    if(isValidValue){
                        materialDialog.getActionButton(DialogAction.POSITIVE).setEnabled(true);
                    }

                    if(keyCode== KeyEvent.KEYCODE_ENTER && isValidValue) {
                        MaskedInputDialog.this.materialDialog.getActionButton(DialogAction.POSITIVE).performClick();
                        return true;
                    }else {
                        return false;
                    }
                }
            });
        }
    }

    /**
     * Verifica se o usuário informou a quantidade correta de caracteres conforme a mascara
     */
    private boolean isValidValue(String unmaskedText) {
        boolean isValid = true;
//        if(unmaskedText != null){
//            isValid = unmaskedText.length() >= symbolCount;
//        }

        return isValid;
    }

    private int getSymbolCount(){
        Pattern p = Pattern.compile("\\"+notMaskedSymbol);
        Matcher m = p.matcher(mask);
        int count = 0;
        while (m.find()){
            count +=1;
        }
        return count;
    }
}
