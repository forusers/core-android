package br.com.forusers.core.android.common.fragment;

/**
 * Created by hellmann on 14/07/16.
 */
public interface IActivityToolbarBehaviour {
    void setToolBarTitle(String title);
}
