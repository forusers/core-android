package br.com.forusers.core.android.common.view;

import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import br.com.forusers.core.android.*;

/**
 * Created by hellmanss on 28/07/16.
 */
public class DefaultRowHolder extends RecyclerView.ViewHolder  {
    public TextView terciaryText;
    public View mainView;
    public View layoutHeader;
    public TextView secaoTextView;
    public TextView primaryText;
    public TextView secondaryText;
    public TextView rightText;

    public DefaultRowHolder(View itemView) {
        super(itemView);
        mainView = itemView.findViewById(R.id.defaultRow_layout );
        layoutHeader = itemView.findViewById(R.id.defaultRow_header);
        secaoTextView = (TextView)itemView.findViewById(R.id.defaultRow_secao);
        primaryText = (TextView)itemView.findViewById(R.id.defaultRow_primaryText);
        secondaryText = (TextView)itemView.findViewById(R.id.defaultRow_secondaryText);
        terciaryText = (TextView)itemView.findViewById(R.id.defaultRow_terciaryText);
        rightText = (TextView)itemView.findViewById(R.id.defaultRow_rightText);
    }
}
