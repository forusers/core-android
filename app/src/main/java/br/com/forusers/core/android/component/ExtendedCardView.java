package br.com.forusers.core.android.component;

import android.content.*;
import android.content.res.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import br.com.forusers.core.android.*;


/**
 * Created by hellmann on 10/03/16.
 */
public class ExtendedCardView extends LinearLayout{

    private final LinearLayout mContentView;
    private final TextView tvTitle;

    public ExtendedCardView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ExtendedCardView, 0, 0);
        String title = a.getString(R.styleable.ExtendedCardView_label);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.extended_card_view, this, true);

        tvTitle = (TextView) view.findViewById(R.id.cardViewMS_title);

        if(title != null && !title.trim().equals(""))
            tvTitle.setText(title);
        else
            tvTitle.setVisibility(GONE);

        mContentView = (LinearLayout)view.findViewById(R.id.cardViewMs_container);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if(mContentView == null){
           super.addView(child, index, params);
        }else{
            //Adiciona ao meu container de itens
            mContentView.addView(child, index, params);
        }
    }

    public void setText(String text){
        tvTitle.setText(text);
    }

    public TextView getTextView(){
        return tvTitle;
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);
    }
}
