package br.com.forusers.core.android.common.fragment;

import android.app.*;
import android.os.*;
import android.support.annotation.*;
import android.support.design.widget.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.*;
import android.view.*;


import com.afollestad.materialdialogs.*;

import br.com.forusers.core.android.*;
import br.com.forusers.core.android.R;
import br.com.forusers.core.android.R.*;
import br.com.forusers.core.android.component.*;

public abstract class AbstractFragment extends Fragment {
    public final String TAG = this.getClass().getSimpleName();
    private int mTitle;
    protected String name;
    protected int mLayoutId;
    protected FloatingActionButton fab;
    MaterialDialog mProgressBar;
    private IActivityToolbarBehaviour activityBehaviour;

    protected Exception exception;
    private MaterialDialog.Builder progressBarBuilder;

    public AbstractFragment(@LayoutRes int layoutId, @StringRes int title) {
        this.mLayoutId =layoutId;
        this.mTitle = title;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(this.mLayoutId, container, false);

        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            this.fab = (FloatingActionButton) view.getRootView().findViewById(id.defaultFab_fab);
            if(fab != null) {
                fab.setOnClickListener(null);
                fab.setVisibility(View.GONE);
            }

            progressBarBuilder = new MaterialDialog.Builder(view.getContext());
            progressBarBuilder.title(R.string.progressBar_aguarde);
            progressBarBuilder.content(R.string.progressBar_consultando);
            progressBarBuilder.progress(true, 100);

        } catch (Exception e) {
            DialogBuilder.e(getContext(), e);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity() != null){
            if(getActivity() instanceof IActivityToolbarBehaviour){
                activityBehaviour = (IActivityToolbarBehaviour) getActivity();
            }
        }

        setTitle();

        this.init();
    }

    private void setTitle() {
        if(activityBehaviour != null){
            activityBehaviour.setToolBarTitle(getString(mTitle));
        }
    }

    protected void init() {
        View view = this.getView();
        this.initComponent(view);
        this.initListeners();
        this.initData();
    }

    public abstract void initComponent(View var1);
    public abstract void initListeners();
    public abstract void initData();

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected void backFragment() {
        this.getActivity().onBackPressed();
    }

    public void showProgressBar(Boolean show) {
            if(show) {
                mProgressBar = progressBarBuilder.show();
            } else {
                if(mProgressBar != null) {
                    mProgressBar.dismiss();
                    mProgressBar = null;
                }
            }
    }

    public boolean openFragment(Activity activity){
        return openFragment(activity, false);
    }
    public boolean openFragment(Activity activity, boolean addToBackStack){
        return AbstractFragment.openFragment(activity, this, addToBackStack);
    }

    public static boolean openFragment(Activity activity, Fragment fragment, boolean addToBackStack) {
        String fragmentTag = fragment.getClass().getName();
        FragmentManager manager = ((AppCompatActivity) activity).getSupportFragmentManager();
        manager.executePendingTransactions();

        boolean fragmentPopped=false;
        //        boolean fragmentPopped = manager.popBackStackImmediate(fragmentTag , 0);

        FragmentTransaction ftx = manager.beginTransaction();

        Fragment oldFragment = manager.findFragmentByTag(fragmentTag);

        if (!fragmentPopped && oldFragment == null) {

            if(addToBackStack){
                ftx.addToBackStack(fragmentTag);
            }else{
                for(int i = 0; i < manager.getBackStackEntryCount(); ++i) {
                    manager.popBackStack();
                }
            }

            ftx.replace(R.id.fragmentContainer_layout, fragment, fragmentTag);
            ftx.commit();

            return true;
        }else{
            return false;
        }
    }

    public void showFab(boolean show){
        if(fab != null) {
            fab.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public String getStringResource(@StringRes int res, Object... parmArgs){
        return getContext().getString(res, parmArgs);
    }

    public Exception getException() {
        return exception;
    }

    public IActivityToolbarBehaviour getActivityBehaviour() {
        return activityBehaviour;
    }

    public void setTitle(@StringRes int title) {
        this.mTitle = title;
    }
}

