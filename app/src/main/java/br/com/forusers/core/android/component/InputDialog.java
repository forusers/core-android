package br.com.forusers.core.android.component;

import android.content.*;
import android.support.annotation.*;
import android.text.*;
import android.view.*;

import com.afollestad.materialdialogs.*;

import br.com.forusers.core.android.*;

/**
 * Created by hellmann on 30/03/16.
 */
public class InputDialog implements MaterialDialog.InputCallback {
    private Context mContext;
    private Object mListener;
    private MaterialDialog.Builder mBuilder;
    private String value;
    private String oldValue;
    private String mTitle;
    private int mInputType = -1;
    private String mHint;


    private MaterialDialog dialog;


    public interface OnChangeStringListener{
        void onChangeStringValue(InputDialog inputDIalog, String value);
    }
    public interface OnChangeDoubleListener{
        void onChangeDoubleValue(InputDialog inputDIalog, Double value);
    }
    public interface OnChangeIntegerListener{
        void onChangeIntegerValue(InputDialog inputDIalog, Integer value);
    }
    public interface OnChangeLongListener{
        void onChangeLongValue(InputDialog inputDIalog, Long value);
    }

    public InputDialog(Context context, String hint, Integer pValue, OnChangeIntegerListener listener) {
        this.value = pValue != null ? pValue.toString() : "";
        mListener = listener;
        mContext = context;
        mHint = hint != null ? hint : "";

        mInputType = InputType.TYPE_CLASS_NUMBER;
        init();
    }

    public InputDialog(Context context, String hint, Long pValue, OnChangeLongListener listener) {
        this.value = pValue != null ? pValue.toString() : "";
        mListener = listener;
        mContext = context;
        mHint = hint != null ? hint : "";

        mInputType = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL;
        init();
    }
    public InputDialog(Context context, String hint, Double pValue, OnChangeDoubleListener listener) {
        this.value = pValue != null ? pValue.toString() : "";
        mListener = listener;
        mContext = context;
        mHint = hint != null ? hint : "";

        mInputType = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL;
        init();
    }
    public InputDialog(Context context, String hint, String pValue, OnChangeStringListener listener) {
        this(context, hint, pValue, false, listener);
    }

    public InputDialog(Context context, String hint, String pValue, boolean multiline, OnChangeStringListener listener) {
        this.value = pValue != null ? pValue : "";
        mListener = listener;
        mContext = context;
        mHint = hint != null ? hint : "";


        if(multiline){
            mInputType =InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
        }else{
            mInputType = InputType.TYPE_CLASS_TEXT;
        }

        init();
    }



    private void init() {
        mBuilder = new MaterialDialog.Builder(mContext);

//        mBuilder.onPositive(this);

        //Quando for String, o valor no campo já deve vir preenchido
        String prefill = mListener != null && mListener instanceof OnChangeStringListener ? value : null;

        if(mInputType != -1)
            mBuilder.inputType(mInputType);

        mBuilder.input(value, prefill, true, this);


        //se nao existir title então utilizar o hint
        if(mTitle == null || !mTitle.trim().isEmpty()){
            if(mHint != null)
            mTitle = mHint;
        }

        //title
        if(mTitle != null)
            mBuilder.title(mTitle);

        //cancelar
        mBuilder.negativeText(mContext.getString(br.com.forusers.core.android.R.string.cancelar));
        mBuilder.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        });
    }


    public void show(){
        dialog = mBuilder.show();

        if(dialog.getInputEditText() != null)
            dialog.getInputEditText().selectAll();

        //evento ao clicar em OK/DONE
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode== KeyEvent.KEYCODE_ENTER) {
                    InputDialog.this.dialog.getActionButton(DialogAction.POSITIVE).performClick();
                    return true;
                }else {
                    return false;
                }
            }
        });

//        if(lines != null){
//            if(dialog.getInputEditText() != null) {
//                dialog.getInputEditText().setLines(lines);
//            }
//        }

    }

    public InputDialog title(String title){
        this.mTitle = title;
        mBuilder.title(mTitle);
        return this;
    }

    public InputDialog inputType(int inputType){
        this.mInputType = inputType;
        mBuilder.inputType(mInputType);
        return this;
    }

    @Override
    public void onInput(MaterialDialog dialog, CharSequence input) {
        value = input == null ? null : input.toString();

        //Quando utilizado o KeyListener para forçar o evento, este é disparado duas vezes pela MaterialDialog
        //Visto que é uma biblioteca de terceiros, foi feito a validação abaixo para tratar este evento
        if(value != null) {

            if(oldValue == null || !value.equals(oldValue)) {

                oldValue=value;


                if (mListener instanceof OnChangeDoubleListener) {
                    Double val = value == null || value.trim().isEmpty() ? 0.0 : Double.valueOf(value);
                    ((OnChangeDoubleListener) mListener).onChangeDoubleValue(this, val);

                } else if (mListener instanceof OnChangeStringListener) {
                    ((OnChangeStringListener) mListener).onChangeStringValue(this, value);

                } else if (mListener instanceof OnChangeLongListener) {
                    Long val = value == null || value.trim().isEmpty() ? 0l : Long.valueOf(value);
                    ((OnChangeLongListener) mListener).onChangeLongValue(this, val);

                } else if (mListener instanceof OnChangeIntegerListener) {
                    Integer val = value == null || value.trim().isEmpty() ? 0 : Integer.valueOf(value);
                    ((OnChangeIntegerListener) mListener).onChangeIntegerValue(this, val);
                }
            }
        }
    }


    public String getValue() {
        return value;
    }

    public Double getValueAsDouble(){
        return Double.valueOf(value);
    }
    public Long getValueAsLong(){
        return Long.valueOf(value);
    }
    public Integer getValueAsInteger(){
        return Integer.valueOf(value);
    }

    public MaterialDialog getDialog() {
        return dialog;
    }
}

