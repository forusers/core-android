package br.com.forusers.core.android.component;

import android.content.*;
import android.support.annotation.*;
import android.support.design.widget.*;
import android.text.*;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.*;

import java.util.*;

import br.com.forusers.core.android.R;

/**
 * Caixa de dialogo para informar a sugestão. Utilizar desta forma:
 new SugestaoInputDialog(getContext(), "Informe a sugestão", 15.0, 16.0, 0.0).show(new SugestaoInputDialog.OnChangeSugestaoListener() {
    @Override
    public void onChangeSugestao(Double sugestao) {
        //retorno da caixa de dialogo
    }
});
 */
public class ExtendedInputDialog {

    private final String title;
    private final Context context;
    private final Double input;
    private MaterialDialog materialDialog;
    private View rootView;
    private TextInputLayout edValor;

    private List<TextInputLayout> viewList = new ArrayList<>();

    private OnChangeValorListener listener;
    private int mInputType=InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL;

    public interface OnChangeValorListener {
        void onChangeValor(Double sugestao);
    }

    public ExtendedInputDialog(Context context, String title, Double pInput){
        this.title=title;
        this.context=context;
        this.input = pInput;
    }

    public ExtendedInputDialog addView(String hint, String valor){
        TextInputLayout layout = new TextInputLayout(context);
        layout.setHint(hint);

        TextInputEditText editText = new TextInputEditText(context);
//        EditText editText = new EditText(context);
        editText.setText(valor);
//        editText.setHint(hint);
        layout.addView(editText);
        viewList.add(layout);
        return this;
    }

    public void show(OnChangeValorListener listener){
        this.listener = listener;
        init();
    }

    private void init() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this.context);
        builder.customView(R.layout.extended_input_dialog, false);

        if(title != null)
            builder.title(this.title);

        builder.negativeText(R.string.cancelar);
        builder.positiveText(R.string.ok);
        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                if(listener != null){
                    if(edValor.getEditText() != null) {
                        String strVal = edValor.getEditText().getText().toString();
                        double val = 0.0;
                        if (!strVal.trim().isEmpty()) {
                            val = Double.valueOf(strVal);
                        }
                        listener.onChangeValor(val);
                    }
                }
            }
        });
        this.materialDialog = builder.show();
        this.rootView = this.materialDialog.getCustomView();

        if (this.rootView != null) {
            LinearLayout layoutExetended = (LinearLayout) rootView.findViewById(R.id.layoutExtended);
            for(TextInputLayout view : viewList){

                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                param.rightMargin=5;

                view.setLayoutParams(param);

                layoutExetended.addView(view);
            }

            this.edValor = (TextInputLayout) this.rootView.findViewById(R.id.edValor);

            if(edValor.getEditText() != null) {
                this.edValor.getEditText().setInputType(mInputType);

                if (title != null) {
                    edValor.setHint(title);
                }
                edValor.getEditText().requestFocus();
                this.edValor.getEditText().setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode == 66) {
                            materialDialog.getActionButton(DialogAction.POSITIVE).performClick();
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
            }
        }
    }

    public void setInputType(int inputType) {
        this.mInputType = mInputType;
    }
}
